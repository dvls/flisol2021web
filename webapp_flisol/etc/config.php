<?php
/*------------------------------------------------------------------
INICIO DE SESION
------------------------------------------------------------------*/
session_start();
//----------------------------------------
//! _SESSION['despliegue'] es la variable
//! que tendrá los parámetros de despliegue
//! de la página que el usuario podrá ajustar.
//----------------------------------------
if (isset($_SESSION['despliegue']))
    $_SESSION['despliegue'] = $_SESSION['despliegue'];
else
    $_SESSION['despliegue'] = new stdClass(); //!< valor inicial de despliegue
session_regenerate_id();
ini_set('default_charset', 'UTF-8');

//----------------------------------------
// Se activan TODOS los mensajes de error
// pues el script debe funcionar sin
// generar ningún error.
// Si se despliegan errores es porque hay
// problemas con alguna librería.
//----------------------------------------
// if ($_SESSION['despliegue']->depurando) {
//     ini_set('display_errors', 1);
//     error_reporting(E_ALL);
// } else {
//     ini_set('display_errors', 0);
// }
ini_set('display_errors', 1);
error_reporting(E_ALL);

//------------------------------------------------------------------
//! VARIABLES DE IDENTIFICACIÓN DEL BACKEND
//! ---------------------------------------
//! Tutorial para identificar el código del googlespreadsheet en:
//! https://scottcents.medium.com/how-to-convert-google-sheets-to-json-in-just-3-steps-228fe2c24e6
//! de ahí se obtiene el spreadsheet_id y el tab_id
//----------------------------------------------------------------*/
$spreadsheet_id = "1yEnnjSWJ2dGF0wEFPHrKtfXMUINSCGuaKRqKDhjTdAQ";
$tab_id = "oma37tl";
$titulo = "Programación Flisol Bogotá 2021";
$google_analytics = "UA-48974201-1";

//------------------------------------------------------------------
//! VARIABLES GLOBALES
//----------------------------------------------------------------*/
$meta_variables = "
    <meta name='DC.title' content='Flisol Bogotá 2021' />
    <meta name='geo.region' content='CO-DC' />
    <meta name='geo.placename' content='Bogot&aacute;' />
    <meta name='geo.position' content='4.683695;-74.060115' />
    <meta name='ICBM' content='4.683695;-74.060115' />";

$estilos_css = "
    <link rel='stylesheet' type='text/css' href='css/flisol.css' />";

$estilos_css_adicionales = "
    <link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
    <style>
        body { font-family: 'Rajdhani', sans-serif; }
    </style>";

$javascripts_adicionales = "
    <script>
        $(window).load(function() {
                // Animate loader off screen
                $('.se-pre-con').fadeOut('slow');;
        });
    </script>";

$timezones = array(
      'America/Adak'
    , 'America/Araguaina'
    , 'America/Argentina/Jujuy'
    , 'America/Argentina/Salta'
    , 'America/Argentina/Ushuaia'
    , 'America/Bahia'
    , 'America/Belize'
    , 'America/Boise'
    , 'America/Caracas'
    , 'America/Chihuahua'
    , 'America/Curacao'
    , 'America/Denver'
    , 'America/Eirunepe'
    , 'America/Glace_Bay'
    , 'America/Grenada'
    , 'America/Guyana'
    , 'America/Indiana/Indianapolis'
    , 'America/Indiana/Tell_City'
    , 'America/Inuvik'
    , 'America/Kentucky/Louisville'
    , 'America/Lima'
    , 'America/Managua'
    , 'America/Matamoros'
    , 'America/Metlakatla'
    , 'America/Monterrey'
    , 'America/New_York'
    , 'America/North_Dakota/Beulah'
    , 'America/Panama'
    , 'America/Port-au-Prince'
    , 'America/Punta_Arenas'
    , 'America/Regina'
    , 'America/Santiago'
    , 'America/Sitka'
    , 'America/St_Lucia'
    , 'America/Tegucigalpa'
    , 'America/Toronto'
    , 'America/Winnipeg'
    , 'America/Anchorage'
    , 'America/Argentina/Buenos_Aires'
    , 'America/Argentina/La_Rioja'
    , 'America/Argentina/San_Juan'
    , 'America/Aruba'
    , 'America/Bahia_Banderas'
    , 'America/Blanc-Sablon'
    , 'America/Cambridge_Bay'
    , 'America/Cayenne'
    , 'America/Costa_Rica'
    , 'America/Danmarkshavn'
    , 'America/Detroit'
    , 'America/El_Salvador'
    , 'America/Godthab'
    , 'America/Guadeloupe'
    , 'America/Halifax'
    , 'America/Indiana/Knox'
    , 'America/Indiana/Vevay'
    , 'America/Iqaluit'
    , 'America/Kentucky/Monticello'
    , 'America/Los_Angeles'
    , 'America/Manaus'
    , 'America/Mazatlan'
    , 'America/Mexico_City'
    , 'America/Montevideo'
    , 'America/Nipigon'
    , 'America/North_Dakota/Center'
    , 'America/Pangnirtung'
    , 'America/Port_of_Spain'
    , 'America/Rainy_River'
    , 'America/Resolute'
    , 'America/Santo_Domingo'
    , 'America/St_Barthelemy'
    , 'America/St_Thomas'
    , 'America/Thule'
    , 'America/Tortola'
    , 'America/Yakutat'
    , 'America/Anguilla'
    , 'America/Argentina/Catamarca'
    , 'America/Argentina/Mendoza'
    , 'America/Argentina/San_Luis'
    , 'America/Asuncion'
    , 'America/Barbados'
    , 'America/Boa_Vista'
    , 'America/Campo_Grande'
    , 'America/Cayman'
    , 'America/Creston'
    , 'America/Dawson'
    , 'America/Dominica'
    , 'America/Fort_Nelson'
    , 'America/Goose_Bay'
    , 'America/Guatemala'
    , 'America/Havana'
    , 'America/Indiana/Marengo'
    , 'America/Indiana/Vincennes'
    , 'America/Jamaica'
    , 'America/Kralendijk'
    , 'America/Lower_Princes'
    , 'America/Marigot'
    , 'America/Menominee'
    , 'America/Miquelon'
    , 'America/Montserrat'
    , 'America/Nome'
    , 'America/North_Dakota/New_Salem'
    , 'America/Paramaribo'
    , 'America/Porto_Velho'
    , 'America/Rankin_Inlet'
    , 'America/Rio_Branco'
    , 'America/Sao_Paulo'
    , 'America/St_Johns'
    , 'America/St_Vincent'
    , 'America/Thunder_Bay'
    , 'America/Vancouver'
    , 'America/Yellowknife'
    , 'America/Antigua'
    , 'America/Argentina/Cordoba'
    , 'America/Argentina/Rio_Gallegos'
    , 'America/Argentina/Tucuman'
    , 'America/Atikokan'
    , 'America/Belem'
    , 'America/Bogota'
    , 'America/Cancun'
    , 'America/Chicago'
    , 'America/Cuiaba'
    , 'America/Dawson_Creek'
    , 'America/Edmonton'
    , 'America/Fortaleza'
    , 'America/Grand_Turk'
    , 'America/Guayaquil'
    , 'America/Hermosillo'
    , 'America/Indiana/Petersburg'
    , 'America/Indiana/Winamac'
    , 'America/Juneau'
    , 'America/La_Paz'
    , 'America/Maceio'
    , 'America/Martinique'
    , 'America/Merida'
    , 'America/Moncton'
    , 'America/Nassau'
    , 'America/Noronha'
    , 'America/Ojinaga'
    , 'America/Phoenix'
    , 'America/Puerto_Rico'
    , 'America/Recife'
    , 'America/Santarem'
    , 'America/Scoresbysund'
    , 'America/St_Kitts'
    , 'America/Swift_Current'
    , 'America/Tijuana'
    , 'America/Whitehorse'
    , 'Europe/Amsterdam'
    , 'Europe/Belgrade'
    , 'Europe/Bucharest'
    , 'Europe/Copenhagen'
    , 'Europe/Helsinki'
    , 'Europe/Kaliningrad'
    , 'Europe/Ljubljana'
    , 'Europe/Malta'
    , 'Europe/Moscow'
    , 'Europe/Prague'
    , 'Europe/San_Marino'
    , 'Europe/Skopje'
    , 'Europe/Tirane'
    , 'Europe/Vatican'
    , 'Europe/Warsaw'
    , 'Europe/Andorra'
    , 'Europe/Berlin'
    , 'Europe/Budapest'
    , 'Europe/Dublin'
    , 'Europe/Isle_of_Man'
    , 'Europe/Kiev'
    , 'Europe/London'
    , 'Europe/Mariehamn'
    , 'Europe/Oslo'
    , 'Europe/Riga'
    , 'Europe/Sarajevo'
    , 'Europe/Sofia'
    , 'Europe/Ulyanovsk'
    , 'Europe/Vienna'
    , 'Europe/Zagreb'
    , 'Europe/Astrakhan'
    , 'Europe/Bratislava'
    , 'Europe/Busingen'
    , 'Europe/Gibraltar'
    , 'Europe/Istanbul'
    , 'Europe/Kirov'
    , 'Europe/Luxembourg'
    , 'Europe/Minsk'
    , 'Europe/Paris'
    , 'Europe/Rome'
    , 'Europe/Saratov'
    , 'Europe/Stockholm'
    , 'Europe/Uzhgorod'
    , 'Europe/Vilnius'
    , 'Europe/Zaporozhye'
    , 'Europe/Athens'
    , 'Europe/Brussels'
    , 'Europe/Chisinau'
    , 'Europe/Guernsey'
    , 'Europe/Jersey'
    , 'Europe/Lisbon'
    , 'Europe/Madrid'
    , 'Europe/Monaco'
    , 'Europe/Podgorica'
    , 'Europe/Samara'
    , 'Europe/Simferopol'
    , 'Europe/Tallinn'
    , 'Europe/Vaduz'
    , 'Europe/Volgograd'
    , 'Europe/Zurich'
    );
sort($timezones);

$lista_blanca_request = array(
      'tzone' => $timezones
    , 'fecha' => array() // se cuadran con las fechas de las actividades
);
$defecto_request = array('tzone' => date_default_timezone_get());
// $modulos_publicos  = array_merge($modulos_publicos, array(array("flisol", "programacion")));
if (!isset($_SESSION['despliegue']->tzone))
    $_SESSION['despliegue']->tzone = 'America/Bogota';
date_default_timezone_set($_SESSION['despliegue']->tzone);
if (!isset($_SESSION['despliegue']->fecha))
    $_SESSION['despliegue']->fecha = date("Ymd");

$html_encabezado = "
        <h1>Programaci&oacute;n FLISoL 2021</h1                                                                                                                                
        ><div class='DISCLAMER'
            >Programación sujeta a posibles cambios, por favor revisar &eacute;sta
            agenda de actividades antes de cada actividad.
        </div
        >";

?>
