<?
/*------------------------------------------------------------------*/
function cuadrar_despliegue()
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DESCRIPCION:
ENTRADAS:
SALIDAS:
PRE:
POST:
--------------------------------------------------------------------*/
{
    global $lista_blanca_request, $defecto_request;

    foreach ($_REQUEST as $llave => $valor) {

        //----------------------------------------
        // Se inicializan las variables que no
        // tengan valor antes de procesarla
        //----------------------------------------
        if (!isset($_SESSION['despliegue']->$llave))
            if (isset($defecto_request[$llave]))
                $_SESSION['despliegue']->$llave = $defecto_request[$llave];

        //----------------------------------------
        // Si el dato está en la lista blanca de
        // posibilidades se actualiza el valor.
        //----------------------------------------
        if (isset($lista_blanca_request[$llave]))
            if (in_array($valor, $lista_blanca_request[$llave])) {
                $_SESSION['despliegue']->$llave = $valor;
                if ($llave == 'tzone')
                    date_default_timezone_set($valor);
            }
    }
}


/*------------------------------------------------------------------*/
function Mostrar($dato, $mensaje = "")
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DESCRIPCION:
ENTRADAS:
SALIDAS:
PRE:
POST:
--------------------------------------------------------------------*/
{
    if ($mensaje != "")
        $retorno = "<div>$mensaje<pre>".htmlentities(var_export($dato, true))."</pre></div>";
    else
        $retorno = "<pre>".htmlentities(var_export($dato, true))."</pre>";

    return $retorno;
}


/*------------------------------------------------------------------*/
function arregloxcampo($datos, $campo, $delimitador = null, $principio = array())
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DESCRIPCION:
ENTRADAS:
SALIDAS:
PRE:
POST:
--------------------------------------------------------------------*/
{
    $retorno = array();
    $termino = false;

    foreach ($datos as $llave => $valor) {
        if (is_object($valor->$campo))
            if (get_class($valor->$campo) == "DateTime") {
                $retorno[] = $valor->$campo->format($delimitador);
                $termino = true;
            }

        if ($delimitador != null && !$termino) {
            $campos = preg_split($delimitador, $valor->$campo);
            foreach ($campos as $vl)
                if (!in_array($vl, $principio))
                    $retorno[] = trim($vl);
        } else if (!$termino)
            if (!in_array($valor->$campo, $principio))
                $retorno[] = trim($valor->$campo);
    }
    $retorno = array_unique($retorno);
    sort($retorno);
    $retorno = array_merge($principio, $retorno);

    return $retorno;
}


?>
