<?php
//==================================================================
//! \mainpage Visor de Programacion de Eventos desde Google Spreadsheet en json
//! \section licence Licencia
//! Copyright (C) 2019 Ricardo Naranjo Faccini - Skina IT Solutions
//! https://www.skinait.com/
//!
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU General Public License as published by
//! the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//!
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU General Public License for more details.
//!
//! You should have received a copy of the GNU General Public License
//! along with this program.\n  If not, see <https://www.gnu.org/licenses/>.
//!
//! ================================================================
//! \section usage EJEMPLO DE USO:
//! <a href='https://skinait.com/flisol2021/' target='AUX'>https://skinait.com/flisol2021/</a>
//!
//! \section instructions INSTRUCCIONES DE USO:
//! Genere una hoja de cálculo en Google Spreadsheets con las
//! siguientes columnas:
//! 
//! \subsection columns Columnas
//! Ciudad que representa \n 
//! Nombre de la actividad \n 
//! Fecha y hora de la actividad (GMT) \n 
//! Duración de la actividad \n 
//! Canal de youtube donde se transmitirá la actividad \n 
//! Lenguaje en que se brindará la actividad \n 
//! Tipo de actividad \n 
//! Nombre del ponente (encargado de la actividad) \n 
//! Medio de contacto con el ponente (ojalá @Telegram) \n 
//! Temáticas de la actividad \n 
//! Telegram del organizador de la ciudad/país \n 
//! Descripción de la actividad \n 
//! Foto del encargado \n 
//! Perfil del encargado \n 
//! URL de las memorias \n 
//!
//! ingrese a etc/config.php para cuadrar los valores iniciales de
//! los parámetros de configuración incluyendo el código de la hoja
//! de cálculo, de la pestaña y el código google que le permita ver
//! los datos de la hoja de cálculo en formato json.
//!
//==================================================================

//----------------------------------------
//! PARAMETROS DE CONFIGURACIÓN
//! De los parámetros de configuración vienen
//! todos los datos particulares del evento
//! para la ciudad que los va a presentar.
//----------------------------------------
require_once("etc/config.php");
require_once("lib/flisol.php");
cuadrar_despliegue();
//----------------------------------------
//! Se carga la información del google
//! spreadsheet
//----------------------------------------
$hojadecalculo = "https://spreadsheets.google.com/feeds/list/"
               . $spreadsheet_id
               . "/"
               . $tab_id
               . "/public/values?alt=json";
$degoogle = file_get_contents($hojadecalculo);

//----------------------------------------
//! Se toma el json de Google y se convierte
//! en un arreglo de php.
//----------------------------------------
$datos = simplificar_json_google($degoogle);

//----------------------------------------
$contenido = (isset($contenido) ? $contenido : "");

//----------------------------------------
//! Se ordena el arreglo $datos
//----------------------------------------
$datos = ordenar_actividades($datos); //< obtiene las actividades ordenadas de acuerdo con el horario
$salones = arregloxcampo($datos, "Lugar", null, array("Latinoamérica")); //< arreglo con las ciudades (salones)
$temas = arregloxcampo($datos, "tema", "#(?<!Free), #"); //< Arreglo con las temáticas.
$conferencistas = arregloxcampo($datos, "Nombres y apellidos"); //< arreglo con los conferencistas.
$actividades = arregloxcampo($datos, "Actividad"); //< arreglo con las actividades programadas.
$inicios = arregloxcampo($datos, "Inicio", "YmdH"); //< arreglo con las fecha-hora de inicio de la actividad.
$botonera = "";

$fechas = arregloxcampo($datos, "Inicio", "Ymd");
$lista_blanca_request['fecha'] = $fechas;
if (!in_array($_SESSION['despliegue']->fecha, $fechas))
    $_SESSION['despliegue']->fecha = $fechas[0];

//----------------------------------------
//! Se procesa nuevamente el _REQUEST
//! después de cargar las listas blancas
//! con las fechas.
//----------------------------------------
cuadrar_despliegue();

//----------------------------------------
//! Botón para elegir la zona horaria del
//! despliegue.
//----------------------------------------
$filtro_tzone = "";
foreach ($timezones as $llave => $valor) {
    $selected = "";
    if ($valor == $_SESSION['despliegue']->tzone)
        $selected = " selected='selected'";
    $filtro_tzone .= "<option value='$valor'$selected>$valor</option>";
}
$script = "window.open('?tzone='+this.value, '_top');";
$filtro_tzone = "<select onChange=\"$script\">$filtro_tzone</select>";
$botonera.= "<div title='Elija su zona horaria' class='FECHA'>$filtro_tzone</div>";

//----------------------------------------
//! Botón para elegir la fecha de las
//! actividades a desplegar.
//----------------------------------------
$filtro_fecha = "";
foreach ($fechas as $llave => $valor) {
    $valfecha = str_split($valor, 2);
    $valfecha = $valfecha[0].$valfecha[1]."-".$valfecha[2]."-".$valfecha[3];
    $selected = "";
    if ($valor == $_SESSION['despliegue']->fecha)
        $selected = " selected='selected'";
    $filtro_fecha .= "<option value='$valor'$selected>$valfecha</option>";
}
$script = "window.open('?fecha='+this.value, '_top');";
$filtro_fecha = "<select onChange=\"$script\">$filtro_fecha</select>";
$botonera.= "<div title='Elija el día de las actividades' class='FECHA'>$filtro_fecha</div>";

//----------------------------------------
//! Botón para retirar los filtros
//----------------------------------------
$script = "$('.ACTIVIDAD').show('fade');"
        . "$('#flttma').val($('#flttma option:first').val());"
        . "$('#fltcnf').val($('#fltcnf option:first').val());"
        . "$('#fltsln').val($('#fltsln option:first').val());"
        ;
$sin_filtro = "<button onClick=\"$script\">Mostrar todas las actividades del día</button><br />";
$botonera.= "<div class='MAESTRA'>$sin_filtro</div><br />";

//----------------------------------------
//! Se construye un <select> para elegir
//! únicamente las actividades asociadas
//! con un salón.
//----------------------------------------
$filtro_salon = "<option value='cualquiera'>Progama de cualquier ciudad</option>";
foreach ($salones as $llave => $valor)
    $filtro_salon.= "<option value=\"$llave\">"
                  .     "Programación ".$valor
                  . "</option>";

$script = "$('.ACTIVIDAD').hide();"
        . "var salon = '.SLN_0, .SLN_'+this.value;"
        . "$(salon).show('fade');"
        . "$('#flttma').val($('#flttma option:first').val());"
        . "$('#fltcnf').val($('#fltcnf option:first').val());"
        ;
$script = "if (this.value == 'cualquiera') { $('.ACTIVIDAD').show('fade'); } else { $script }";
$filtro_salon = "<div class='CONT_FLTSLN'><select id='fltsln' onChange=\"$script\">$filtro_salon</select></div>";
$botonera .= $filtro_salon;

//----------------------------------------
//! Se construye un <select> para elegir
//! únicamente las actividades asociadas
//! con un tema.
//----------------------------------------
$filtro_tema = "<option value='cualquiera'>Cualquier Tema</option>";
foreach ($temas as $llave => $valor)
    $filtro_tema.= "<option value=\"$llave\">$valor</option>";

$script = "$('.ACTIVIDAD').hide();"
        . "var tema = '.TMA_'+this.value;"
        . "$(tema).show('fade');"
        . "$('#fltcnf').val($('#fltcnf option:first').val());"
        . "$('#fltsln').val($('#fltsln option:first').val());"
        ;
$script = "if (this.value == 'cualquiera') { $('.ACTIVIDAD').show('fade'); } else { $script }";
$filtro_tema = "<div class='CONT_FLTTMA'><select id='flttma' onChange=\"$script\">$filtro_tema</select></div>";
$botonera .= $filtro_tema;

//----------------------------------------
//! Se construye un <select> para elegir
//! únicamente las actividades asociadas
//! con un conferencista.
//----------------------------------------
$filtro_conferencista = "<option value='cualquiera'>Cualquier Ponente</option>";
foreach ($conferencistas as $llave => $valor)
    $filtro_conferencista.= "<option value=\"$llave\">$valor</option>";

$script = "$('.ACTIVIDAD').hide();"
        . "var conferencista = '.CNF_'+this.value;"
        . "$(conferencista).show('fade');"
        . "$('#flttma').val($('#flttma option:first').val());"
        . "$('#fltsln').val($('#fltsln option:first').val());"
        ;
$script = "if (this.value == 'cualquiera') { $('.ACTIVIDAD').show('fade'); } else { $script }";
$filtro_conferencista = "<div class='CONT_FLTCNF'><select id='fltcnf' onChange=\"$script\">$filtro_conferencista</select></div><br />";
$botonera .= $filtro_conferencista;

//----------------------------------------
//! Se construye una botonera para elegir
//! la franja horaria en la que se desea
//! ver las actividades.
//----------------------------------------
$filtro_hora = "";

//----------------------------------------
for ($i = 0; $i < 24; $i++) {
    $hora = ($i < 10 ? "0".$i : $i);
    $script = "$('#flthra').val('cualquiera'); "
            . "$('.ACTIVIDAD').hide();"
            . "$('.HRA_".$_SESSION['despliegue']->fecha."$hora').show('fade');"
            . "$('#flttma').val($('#flttma option:first').val());"
            . "$('#fltcnf').val($('#fltcnf option:first').val());"
            . "$('#fltsln').val($('#fltsln option:first').val());"
            ;
    $filtro_hora.= "<button onClick=\"$script\">$i:00</button>";
    if ($i == 11)
        $filtro_hora .= "<br />";
}
$filtro_hora = "<div class='CONT_FLTHRA'>$filtro_hora</div>";
$botonera .= $filtro_hora;

$contenido .= "<div class='BOTONERA'>$botonera</div>";
//----------------------------------------
//! Se presentan todas las actividades
//----------------------------------------
foreach ($datos as $idx => $dato) {
    if ($dato->Actividad != "DESCANSO" && $dato->Inicio->format("Ymd") == $_SESSION['despliegue']->fecha) {
        $dato->twitter =
            ($dato->twitter != "" ? ($dato->twitter[0] != '@' ? "@" : "") : "").$dato->twitter;
        $id_salon = array_search($dato->Lugar, $salones);
        $id_confe = array_search($dato->{"Nombres y apellidos"}, $conferencistas);

        $temascharla = preg_split("#(?<!Free), #", $dato->tema);
        $ids_temas = array();
        foreach ($temascharla as $valor)
            $ids_temas[] = array_search($valor, $temas);

        $hora = $dato->Inicio->format('YmdH');
        $hora_ini = $dato->Inicio->format('H:i');
        $hora_fin = $dato->Fin->format('H:i');
        $script_sln = "$('#fltsln').val('$id_salon'); "
                    . "$('.ACTIVIDAD').hide();"
                    . "$('#flttma').val($('#flttma option:first').val());"
                    . "$('#fltcnf').val($('#fltcnf option:first').val());"
                    . "var salon = '.SLN_$id_salon, .SLN_0';"
                    . "$(salon).show('fade');"
                    ;

        $script_tma = array();
        foreach ($ids_temas as $id_tema)
            $script_tma[] = "$('#flttma').val('$id_tema'); "
                          . "$('.ACTIVIDAD').hide();"
                          . "$('#fltcnf').val($('#fltcnf option:first').val());"
                          . "$('#fltsln').val($('#fltsln option:first').val());"
                          . "var salon = '.TMA_$id_tema';"
                          . "$(salon).show('fade');"
                          ;

        $script_cnf = "$('#fltcnf').val('$id_confe'); "
                    . "$('.ACTIVIDAD').hide();"
                    . "$('#flttma').val($('#flttma option:first').val());"
                    . "$('#fltsln').val($('#fltsln option:first').val());"
                    . "var conferencista = '.CNF_$id_confe';"
                    . "$(conferencista).show('fade');"
                    ;
        $script_hra = "$('#flthra').val('cualquiera'); "
                    . "$('#flttma').val($('#flttma option:first').val());"
                    . "$('#fltcnf').val($('#fltcnf option:first').val());"
                    . "$('#fltsln').val($('#fltsln option:first').val());"
                    . "$('.ACTIVIDAD').hide();"
                    . "$('.HRA_$hora').show('fade');"
                    ;
        //----------------------------------------
        // DESPLIEGUE DE CADA ACTIVIDAD
        //----------------------------------------
        $tematicas = "";
        $jsid_tema = "";
        foreach ($temascharla as $llave => $temacharla) {
            $tematicas.= "<div class='TEMA' onClick=\"".$script_tma[$llave]."\">"
                       .     "<p>".nl2br($temacharla)."</p>"
                       . "</div\n>"
                       ;
            $jsid_tema.= " TMA_".$ids_temas[$llave];
        }
        // print Mostrar($ids_temas);
        // print Mostrar($script_tma);
        // print Mostrar($tematicas);

        $contenido .= "
<div class='ACTIVIDAD SLN_$id_salon $jsid_tema CNF_$id_confe HRA_$hora'>
    <div class='TITULO' onClick=\"window.open('".$dato->canal."', 'ACTIVIDAD')\">
        <p>".nl2br($dato->Actividad)."</p>
    </div>
    <div class='CONFERENCISTA' onClick=\"$script_cnf\">
        <p style='text-align: center;'>
          ".($dato->foto != ""
          ? "<img class='FOTOCNF' src='".$dato->foto."' />"
          : "<img class='FOTOCNF' src='img/nofoto.png' />")."
        </p>
        <p>".nl2br($dato->{"Nombres y apellidos"})."</p>
        <p class='TWITTER'>".nl2br($dato->{"twitter"})."</p>
    </div>
    <div class='HORARIO' onClick=\"$script_hra\">
        <div class='INICIO'><p>".$hora_ini."</p></div>
        -
        <div class='FIN'><p>".$hora_fin."</p></div>
    </div>
    $tematicas
    <div class='LUGAR' onClick=\"$script_sln\"><p>"
    .preg_replace("#^(\d\d\d)$#", "Sal&oacute;n $1", $dato->Lugar).
    "</p></div>
    <div class='DESCRIPCION'>
        <p>".nl2br($dato->Descripcion)."</p>
        ".($dato->perfildelcolaborador != ""
          ? "<p class='PERFIL'>".$dato->perfildelcolaborador."</p>"
          : "")."
        ".($dato->memorias != "" && $dato->memorias != "No asignado"
          ? "<p class='MEMORIAS'><a href='".$dato->memorias."' target='new'>M&aacute;s informaci&oacute;n</a></p>"
          : "")."
        ".($dato->comunidad != ""
          ? "<p class='REPRESENTA'>".$dato->comunidad."</p>"
          : "")."
    </div>
</div>
";
    }
}

//----------------------------------------
// DESPLIEGUE DE TODO EL LISTADO EN
// $contenido
//----------------------------------------
$contenido = "<div class='ACTIVIDADES'>".$contenido."</div>";

//----------------------------------------
// Google Analytics
//----------------------------------------
if ($google_analytics != "XX-########-#")
    $codigo_analytics = "
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src='https://www.googletagmanager.com/gtag/js?id=$google_analytics'></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '$google_analytics');
</script>";

$contenido = "<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml' lang='es' xml:lang='es'>
    <head>
        $codigo_analytics

        $estilos_css

        <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'/>
        $meta_variables
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script>

        <title>$titulo</title>

        $estilos_css_adicionales

        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js'></script>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js'></script>
        $javascripts_adicionales
    </head>
    <body>
        $html_encabezado
        <div id='MODULO'>".$contenido."</div>
        <center>
            Desarrolló Ricardo Naranjo Faccini de <a href='https://skinait.com'>Skina IT Solutions</a>
            y LinuxCol, Bogotá, Colombia
        </center>
    </body>
</html>";

print $contenido;

//============================================================
// FUNCIONES PROPIAS DEL MODULO
//============================================================

/*------------------------------------------------------------------*/
function simplificar_json_google($degoogle)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//! DESCRIPCION:
//! Toma la información del google spreadsheet y lo simplifica en un
//! json adecuado.
//------------------------------------------------------------------*/
{
    $retorno = array();
    $contador = 0;

    $degoogle = json_decode($degoogle);
    foreach ($degoogle->feed->entry as $llave => $valor) {
        // print "<pre>\n".var_export($valor, true)."\n</pre>";
        $elemento = new stdClass();
        $elemento->id                     = ++$contador;
        $elemento->Lugar                  = trim($valor->{"gsx\$ciudadquerepresentacidadequerepresenta"}->{"\$t"});
        $elemento->Actividad              = trim($valor->{"gsx\$nombredelaactividadnomedaatividade"}->{"\$t"});
        $elemento->fechahora              = trim($valor->{"gsx\$fechayhoradelaactividadutcdataehoradaatividadeutc"}->{"\$t"});
        $elemento->duracion               = trim($valor->{"gsx\$duracióndelaactividadduraçãodaatividade"}->{"\$t"});
        list($hora_inicio, $hora_fin) = estandarizar_fecha($elemento->fechahora, $elemento->duracion);
        $elemento->Inicio                 = $hora_inicio;
        $elemento->Fin                    = $hora_fin;
        $elemento->canal                  = trim($valor->{"gsx\$canalpordondesetransmitirálaactividadcanalondeaatividadeserátransmitida"}->{"\$t"});
        $elemento->Descripcion            = trim($valor->{"gsx\$descripcióndelaactividaddescriçãodaatividade"}->{"\$t"});
        $elemento->nivel                  = trim($valor->{"gsx\$niveldelacharlaníveldaatividade"}->{"\$t"});
        $elemento->lenguajes              = trim($valor->{"gsx\$lenguajeenquesebrindarálaactividadidiomaemqueaatividadeseráoferecida"}->{"\$t"});
        $elemento->tipo                   = trim($valor->{"gsx\$tipodeactividadtipodeatividade"}->{"\$t"});
        $elemento->{"Nombres y apellidos"}= trim($valor->{"gsx\$nombredelponenteencargadodelaactividadnomedoministranteencarregadodaatividade"}->{"\$t"});
        $elemento->twitter                = trim($valor->{"gsx\$mediodecontactoconelponenteojalátelegramcontatocomoministrantepreferencialmenteumacontadotelegram"}->{"\$t"});
        $elemento->tema                   = trim($valor->{"gsx\$temáticasdelaactividadtemasdaatividade"}->{"\$t"});
        $elemento->foto                   = trim($valor->{"gsx\$foto"}->{"\$t"});
        $elemento->memorias               = trim($valor->{"gsx\$memoriasregistros"}->{"\$t"});
        $elemento->perfildelcolaborador   = trim($valor->{"gsx\$perfildelcolaboradorperfildocolaborador"}->{"\$t"});
        $elemento->responsable            = trim($valor->{"gsx\$telegramdelorganizadordelaciudadpaístelegramdoorganizadordacidadepaís"}->{"\$t"});
        $elemento->comunidad              = trim($valor->{"gsx\$representaalgunacomunidadoempresarepresentaalgumacomunidadeouempresa"}->{"\$t"});
        $retorno[] = $elemento;
    }

    return $retorno;
}

/*------------------------------------------------------------------*/
function estandarizar_fecha( $fechahora //< La fecha inicial de la actividad
                           , $duracion  //< La duración de la actividad
                           )
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//! DESCRIPCION:
//! Toma la hora de inicio y duración de la actividad del google
//! spreadsheet y genera los objetos DateTime para el inicio y el
//! final de la actividad.
//------------------------------------------------------------------*/
{
    $cambiar = true;
    $inicio = $fechahora;
    $final = null;
    if (trim($inicio) == "")
        $cambiar = false;
    if (!preg_match("#^\d\d?\/\d\d?\/\d\d\d\d \d\d?:\d\d:\d\d$#", $inicio))
        $cambiar = false;
    if (!preg_match("#^\d\d?:\d\d:\d\d$#", $duracion))
        $cambiar = false;

    if ($cambiar) {
        $inicio = preg_split("# #", $inicio);
        $fechainicio = preg_split("#\/#", $inicio[0]);
        $inicio = $fechainicio[2]."-".$fechainicio[0]."-".$fechainicio[1]." ".$inicio[1];
    }

    $zona_UTC = new DateTimeZone("UTC");
    $zona_local = new DateTimeZone(date_default_timezone_get());

    $inicio = new DateTime($inicio, $zona_UTC);
    $final = clone $inicio;

    $desface = $zona_local->getOffset($inicio);
    $desface = DateInterval::createFromDateString((string)$desface."seconds");
    $inicio->add($desface);
    $final->add($desface);

    $duracion = preg_split("#:#", $duracion);
    $duracion = "PT".$duracion[0]."H".$duracion[1]."M".$duracion[2]."S";
    $duracion = new DateInterval($duracion);
    $final->add($duracion);

    return array($inicio, $final);
}

/*------------------------------------------------------------------*/
function ordenar_actividades($datos)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//! DESCRIPCION:
//! Ordena las actividades de acuerdo con la hora de inicio.
//------------------------------------------------------------------*/
{
    for ($i = 0; $i < count($datos); $i ++) {
        for ($j = $i + 1; $j < count($datos); $j++) {
            if ($datos[$i]->Inicio > $datos[$j]->Inicio) {
                $auxiliar = clone $datos[$i];
                $datos[$i] = clone $datos[$j];
                $datos[$j] = clone $auxiliar;
            }
        }
    }

    return $datos;
}
?>
