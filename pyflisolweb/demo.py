#!/usr/bin/env python3
from browser import document, html
import pyflisolweb as flisol
import sys
from io import StringIO
import datetime

document <= html.H2('Programación FLISoL2021')
url, data = flisol.get_data()
activity_list = data['feed']['entry']
total_activities = len(activity_list)
link = html.A()
link <= html.P('Datos en JSON')
link.href=url
document <= link
document <= html.P(str(total_activities)+' actividades en total.')

localTZoffset = -3
localTZ = datetime.timezone(datetime.timedelta(hours=localTZoffset))

lists = flisol.get_lists(activity_list)
dates = sorted(set([d.astimezone(localTZ).date() for d in lists['dates']]))

ul = html.UL()
for day in dates:
    li = html.LI(day.isoformat())
    selected = flisol.select_activities(activity_list,
                                        date_crit=day,
                                        date_ref_crit='on')
    print('sel:', selected)
    outstring = StringIO()
    original_stdout = sys.stdout
    sys.stdout = outstring
    flisol.print_selected(activity_list, selected, newtz=localTZoffset)
    sys.stdout = original_stdout
    li <= html.DIV(outstring.getvalue())
    ul<=li

document <= ul
