Para probar el móduli usando brython, debe ejecutar el siguiente comando
desde el directorio donde se encuentra el módulo pyflisolweb.py,
el archivo brython-test.html y el subdirectorio con el css, para levantar
un mini servidor web local de prueba:

$ python3 -m http.server

luego, usando el navegador web vaya a la dirección local indicada en la
salida del comando anterior.
