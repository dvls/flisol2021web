#!/usr/bin/env python3

# pyflisolweb
#
# Original PHP version:
# Copyright (C) 2019 Ricardo Naranjo Faccini - Skina IT Solutions
# https://www.skinait.com/
#
# This python port's changes and/or improvements:
# Copyright (C) 2021  Daniel Vicente Lühr Sierra - Triangulo Austral Ltda.
# https://www.trianguloaustral.cl/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib import request
import json
import datetime
import sys
from operator import itemgetter

# default config variables
gsheet={'spreadsheet_id': '1yEnnjSWJ2dGF0wEFPHrKtfXMUINSCGuaKRqKDhjTdAQ',
        'tab_id': 'oma37tl',
        'url': 'https://spreadsheets.google.com/feeds/list/',
        'request_parameters': '/public/values?alt=json'}
# no-photo url
nophoto=''

def get_data():
    '''Returns the url and JSON object containgin the google spreadsheet.'''
    
    # create request url from gsheet config variable
    data_url = gsheet['url']
    data_url += gsheet['spreadsheet_id'] + '/' + gsheet['tab_id']
    data_url += gsheet['request_parameters']

    # get json object using the url and decode it
    response = request.urlopen(data_url)
    data_object = response.read()
    # trick for brython specific code
    if 'brython' in sys.executable.lower():
        data_json = json.loads(data_object)
    else:
        encoding = response.info().get_content_charset('utf-8')
        decoded_data = data_object.decode(encoding)
        data_json = json.loads(decoded_data)
    

    # return the url and the json object
    return data_url, data_json

def get_topic_list(topics):
    ## (quick and dirt trick to handle commas in the topics)
    topic_list = topics.replace(',', ';')
    topic_list = topic_list.replace('Ciudadanía; Estado y Tecnologías Libres',
                            'Ciudadanía, Estado y Tecnologías Libres')
    topic_list = topic_list.replace('FLOSS (Free; Libre and Open Source Software)',
                            'FLOSS (Free, Libre and Open Source Software)')
    topic_list = topic_list.split('; ')
    return topic_list

def parse_date(date_str):
    '''Parse google spreadsheet (awful) date string into a datetime object.'''
    date_utc, time_utc = date_str.split(' ')
    utc_mm, utc_dd, utc_yy = date_utc.split('/')
    utc_HH, utc_MM, utc_SS = time_utc.split(':')
    return datetime.datetime(int(utc_yy), int(utc_mm), int(utc_dd),
                             int(utc_HH), int(utc_MM), int(utc_SS),
                             tzinfo=datetime.timezone.utc)

def parse_length(length):
    '''Parse google spreadsheet length into a timedelta object.'''
    length_HH, length_MM, length_SS = length.split(':')
    return datetime.timedelta(hours=int(length_HH),
                              minutes=int(length_MM),
                              seconds=int(length_SS))

def get_lists(activities):
    '''Get lists of dates, topics, places, etc. for all activities.'''
    dates = set()
    topics = set()
    places = set()
    speakers = set()
    coordinators = set()
    languages = set()
    activity_types = set()
    levels = set()
    lengths = set()
    organizations = set()
    
    for activity in activities:
        # get activity info into variables
        place = activity['gsx$ciudadquerepresenta']['$t']
        date_utc = parse_date(activity['gsx$fechayhoradelaactividadutc']['$t'])
        length = parse_length(activity['gsx$duracióndelaactividad']['$t'])
        language = activity['gsx$lenguajeenquesebrindarálaactividad']['$t']
        activity_type = activity['gsx$tipodeactividad']['$t']
        speaker = activity['gsx$nombredelponenteencargadodelaactividad']['$t']
        speaker_contact = activity['gsx$mediodecontactoconelponenteojalátelegram']['$t']
        topic_list = get_topic_list(activity['gsx$temáticasdelaactividad']['$t'])
        coordinator_contact = activity['gsx$telegramdelorganizadordelaciudadpaís']['$t']
        level = activity['gsx$niveldelacharla']['$t']
        organization = activity['gsx$representaalgunacomunidadoempresa']['$t']
        # add content to the sets
        dates.add(date_utc)
        places.add(place)
        speakers.add(speaker+'|'+speaker_contact) # let's hope the | is not used
        coordinators.add(coordinator_contact)
        languages.add(language)
        activity_types.add(activity_type)
        levels.add(level)
        lengths.add(length)
        organizations.add(organization)
        for topic in topic_list:
            topics.add(topic)

    return {'dates': dates,
            'topics': topics,
            'places': places,
            'speakers': speakers,
            'coordinators': coordinators,
            'language': languages,
            'activity_types': activity_types,
            'levels': levels,
            'lengths': lengths,
            'organizations': organizations}

def select_activities(activities,
                      date_crit=None, date_ref_crit=None,
                      time_crit=None, time_ref_crit=None,
                      length_crit=None, length_ref_crit=None,
                      place_crit=None,
                      speaker_crit=None,
                      coordinator_crit=None,
                      language_crit=None,
                      activity_type_crit=None,
                      level_crit=None,
                      topic_crit=None,
                      organization_crit=None):
    '''Select activities matching ALL specified criteria.'''

    utc = datetime.timezone.utc
    selection = list()
    for idx, activity in enumerate(activities):
        # get activity info into variables
        place = activity['gsx$ciudadquerepresenta']['$t']
        date_utc = parse_date(activity['gsx$fechayhoradelaactividadutc']['$t'])
        length = parse_length(activity['gsx$duracióndelaactividad']['$t'])
        language = activity['gsx$lenguajeenquesebrindarálaactividad']['$t']
        activity_type = activity['gsx$tipodeactividad']['$t']
        speaker = activity['gsx$nombredelponenteencargadodelaactividad']['$t']
        speaker_contact = activity['gsx$mediodecontactoconelponenteojalátelegram']['$t']
        topic_list = get_topic_list(activity['gsx$temáticasdelaactividad']['$t'])
        coordinator_contact = activity['gsx$telegramdelorganizadordelaciudadpaís']['$t']
        level = activity['gsx$niveldelacharla']['$t']
        organization = activity['gsx$representaalgunacomunidadoempresa']['$t']

        # inclusion state variable: will be updated by matching against criteria
        # let's start by assuming it will included
        include_in_list = True
        if place_crit:
            if place_crit != place:
                include_in_list &= False
        if speaker_crit:
            if speaker_crit not in (speaker + speaker_contact):
                include_in_list &= False
        if coordinator_crit:
            if coordinator_crit != coordinator_contact:
                include_in_list &= False
        if language_crit:
            if language_crit != language:
                include_in_list &= False
        if activity_type_crit:
            if activity_type_crit != activity_type:
                include_in_list &= False
        if level_crit:
            if level_crit != level:
                include_in_list &= False
        if topic_crit:
            if topic_crit not in topic_list:
                include_in_list &= False
        if organization_crit:
            if organization_crit != organization:
                include_in_list &= False
        if date_crit:
            # compare using utc time
            reference = 'on'
            if date_ref_crit in ['before', 'on', 'after']:
                reference = date_ref_crit
            if reference == 'on':
                if date_crit != date_utc.date():
                    include_in_list &= False
            elif reference == 'before':
                if date_crit >= date_utc.date():
                    include_in_list &= False
            elif reference == 'after':
                if date_crit <= date_utc.date():
                    include_in_list &= False
        if time_crit:
            # compare using utc time
            reference = 'on'
            if time_ref_crit in ['before', 'on', 'after']:
                reference = time_ref_crit
            if reference == 'on':
                if time_crit != date_utc.timetz():
                    include_in_list &= False
            elif reference == 'before':
                if time_crit >= date_utc.timetz():
                    include_in_list &= False
            elif reference == 'after':
                if time_crit <= date_utc.timetz():
                    include_in_list &= False
        if length_crit:
            reference = 'eq'
            if length_ref_crit in ['shorter', 'eq', 'longer']:
                reference = length_ref_crit
            if reference == 'eq':
                if length_crit != length:
                    include_in_list &= False
            elif reference == 'shorter':
                if length_crit > length:
                    include_in_list &= False
            elif reference == 'longer':
                if length_crit < length:
                    include_in_list &= False
            

        if include_in_list:
            # add index to list (and date for sorting)
            selection.append((idx, date_utc))

    if len(selection) == 0:
        return selection
    else:
        idx_list, date_list = zip(*sorted(selection, key=itemgetter(1)))
        return idx_list
            
        
def print_act(activity, newtz=None):
    '''Print a single activity's info in html'''

    # get activity info into variables
    timestamp = activity['gsx$timestamp']['$t']
    place = activity['gsx$ciudadquerepresenta']['$t']
    title = activity['gsx$nombredelaactividad']['$t']
    date_utc = activity['gsx$fechayhoradelaactividadutc']['$t']
    length = activity['gsx$duracióndelaactividad']['$t']
    stream_url = activity['gsx$canalpordondesetransmitirálaactividad']['$t']
    language = activity['gsx$lenguajeenquesebrindarálaactividad']['$t']
    activity_type = activity['gsx$tipodeactividad']['$t']
    speaker = activity['gsx$nombredelponenteencargadodelaactividad']['$t']
    speaker_contact = activity['gsx$mediodecontactoconelponenteojalátelegram']['$t']
    topics = activity['gsx$temáticasdelaactividad']['$t']
    coordinator_contact = activity['gsx$telegramdelorganizadordelaciudadpaís']['$t']
    description = activity['gsx$descripcióndelaactividad']['$t']
    level = activity['gsx$niveldelacharla']['$t']
    photo = activity['gsx$foto']['$t']
    profile = activity['gsx$perfildelcolaborador']['$t']
    materials_url = activity['gsx$memorias']['$t']
    organization = activity['gsx$representaalgunacomunidadoempresa']['$t']

    # process date and time
    utc = datetime.timezone.utc
    date_dt = parse_date(date_utc)
    delta = parse_length(length)
    enddate_dt = date_dt + delta
    
    # timezone stuff
    localtz = utc #default
    if newtz:
        if type(newtz) is int:
            localtz = datetime.timezone(offset=datetime.timedelta(hours=newtz))
    # note: None tz returns date in local time, not in the original tz (utc)
    date_begin_local = date_dt.astimezone(localtz)
    date_end_local = enddate_dt.astimezone(localtz)
    time_begin = date_begin_local.time().isoformat('minutes')
    time_end = date_end_local.time().isoformat('minutes')

    # transform topics string into list.
    topics = get_topic_list(topics)
    
    # html output
    act_html_class = 'ACTIVIDAD SLN_ CNF_ HRA_'
    print('<div class="'+act_html_class+'">')
    # print title
    print('  <div class="TITULO" onclick="window.open(\'' + stream_url
          + '\', \'ACTIVIDAD\')">')
    print('    <p>' + title + '</p>')
    print('  </div>')
    # print speaker
    onclick = ''
    print('  <div class="CONFERENCISTA" onclick="' + onclick + '">')
    print('    <p style="text-align: center;">')
    if photo:
        print('<img class="FOTOCNF" src="' + photo + '"/>')
    else:
        print('<img class="FOTOCNF" src="' + nophoto + '"/>')
    print('    </p>')
    print('    <p>' + speaker + '</p>')
    print('    <p class="TWITTER">' + speaker_contact + '</p>')
    print('  </div>')
    # print time
    onclick=''
    print('  <div class="HORARIO" onclick="' + onclick + '">')
    print('    <div class="INICIO"><p>' + time_begin + '</p></div>')
    print('    -')
    print('    <div class="FIN"><p>' + time_end + '</p></div>')
    print('  </div>')
    # print topics
    for topic in topics:
        print('  <div class="TEMA" onclick=""><p>' + topic + '</p></div>')
    # print place
    print('  <div class="LUGAR" onclick="">')
    print('    <p>' + place + '</p></div>')
    # print description
    print('  <div class="DESCRIPCION">')
    print('    <p>' + description + '</p>')
    if profile:
        print('    <p class="PERFIL">' + profile + '</p>')
    if materials_url and (materials_url != 'No asignado'):
        print('    <p class=""MEMORIAS">',
              '<a href="' + materials_url + '"',
              'target="new">M&aacute;s informaci&oacute;n</a></p>')
    if organization:
        print('    <p class="REPRESENTA">' + organization + '</p>')
    print('</div>')
    # close main div
    print('</div>')

def print_selected(activities, selection, newtz=None):
    '''Print selected activities.'''

    print('<div id="MODULO">')
    print('<div class="ACTIVIDADES">')
    for selected in selection:
        activity = activities[selected]
        print_act(activity, newtz)
    print('</div>')
    print('</div>')
    
if __name__ == '__main__':
    event='FLISoL 2021'
    print(event)
    print('gs_id=', gsheet['spreadsheet_id'], '\ntab_id=', gsheet['tab_id'])
    url, data = get_data()
    activity_list = data['feed']['entry']
    print('url=', url)
    total_activities = len(activity_list)
    print('total=', total_activities)
    # print first activity
    print_selected(activity_list, (0,))
    # print all activities
    selected_activities = range(total_activities)
    print_selected(activity_list, selected_activities)
