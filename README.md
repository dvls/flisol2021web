# FLISoL 2021 web apps

Colección de códigos para desplegar la lista de actividades de FLISoL 2021, registradas de manera centralizada.

<dl>
  <dt>webapp_flisol</dt>
  <dd>por Ricardo Naranjo Faccini. Código en PHP.</dd>
  
  <dt>pyflisolweb</dt>
  <dd>por Daniel Vicente Lühr Sierra. Código en Python. Módulo para ser incorporado en una app (ver ejemplo usando Brython). Posee menos funcionalidad que el código en PHP.</dd>
</dl>
